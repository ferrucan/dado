import React, { useRef, useEffect } from 'react';
import * as THREE from 'three';

const SpinningDice = ({ width = '300px', height = '300px' }) => {
  const mountRef = useRef(null);

  useEffect(() => {
    const mount = mountRef.current;

    // Escena
    const scene = new THREE.Scene();

    // Cámara
    const camera = new THREE.PerspectiveCamera(75, mount.clientWidth / mount.clientHeight, 0.1, 1000);
    camera.position.z = 3;

    // Renderizador
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(mount.clientWidth, mount.clientHeight);
    renderer.setClearColor(0x00000f, 0.5); // Color de fondo transparente
    mount.appendChild(renderer.domElement);

    // Crear texturas con números
    const createTextTexture = (text) => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      canvas.width = 256;
      canvas.height = 256;
      context.fillStyle = 'red';
      context.fillRect(0, 0, canvas.width, canvas.height);
      context.fillStyle = 'white';
      context.font = 'bold 150px Arial';
      context.textAlign = 'center';
      context.textBaseline = 'middle';
      context.fillText(text, canvas.width / 2, canvas.height / 2);
      return new THREE.CanvasTexture(canvas);
    };

    const materials = [
      new THREE.MeshBasicMaterial({ map: createTextTexture('1') }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('2') }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('3') }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('4') }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('5') }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('6') }),
    ];

    // Geometría del dado
    const geometry = new THREE.BoxGeometry(2, 2, 2);
    const cube = new THREE.Mesh(geometry, materials);
    scene.add(cube);

    // Animación
    const animate = () => {
      requestAnimationFrame(animate);
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;
      renderer.render(scene, camera);
    };

    animate();

    // Limpiar el efecto
    return () => {
      mount.removeChild(renderer.domElement);
    };
  }, []);

  return <div ref={mountRef} style={{ width, height }} />;
};

export default SpinningDice;
