import { useState } from 'react';

export default function Form() {
  const [firstName, setFirstName] = useState('');
  const [age, setAge] = useState('20');
  const ageAsNumber = Number(age);
  return (
    <>
      <label>
        Nombre:
        <input
          value={firstName}
          onChange={e => setFirstName(e.target.value)}
        />
      </label>
      <label>
        Edad:
        <input
          value={age}
          onChange={e => setAge(e.target.value)}
          type="number"
        />
        <button onClick={() => setAge(ageAsNumber + 10)}>
          Añade 10 años
        </button>
      </label>
      {firstName !== '' &&
        <p>Tu nombre es {firstName}.</p>
      }
      {ageAsNumber > 0 &&
        <p>Tu edad es {ageAsNumber}.</p>
      }
    </>
  );
}
