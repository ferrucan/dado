import React, { useRef, useEffect } from 'react';
import * as THREE from 'three';

const Dode2 = ({ width = '500px', height = '500px' }) => {
  const mountRef = useRef(null);

  useEffect(() => {
    const mount = mountRef.current;

    // Escena
    const scene = new THREE.Scene();

    // Cámara
    const camera = new THREE.PerspectiveCamera(75, mount.clientWidth / mount.clientHeight, 0.1, 1000);
    camera.position.z = 4;

    // Renderizador
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(mount.clientWidth, mount.clientHeight);
    renderer.setClearColor(0x00000, 0.5); // Fondo blanco para una mejor visibilidad
    mount.appendChild(renderer.domElement);

    // Agregar una luz
    const light = new THREE.AmbientLight(0xFFFFFF, 0.3); // Luz ambiental
    scene.add(light);

    // Crear texturas con números
    const createTextTexture = (text) => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      canvas.width = 256;
      canvas.height = 256;
      context.fillStyle = 'red';
      context.fillRect(0, 0, canvas.width, canvas.height);
      context.fillStyle = 'white';
      context.font = 'bold 150px Arial';
      context.textAlign = 'center';
      context.textBaseline = 'middle';
      context.fillText(text, canvas.width / 2, canvas.height / 2);
      return new THREE.CanvasTexture(canvas);
    };

    // Crear las texturas para las caras del dodecaedro
    const materials = [
      new THREE.MeshBasicMaterial({ map: createTextTexture('1'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('2'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('3'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('4'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('5'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('6'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('7'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('8'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('9'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('10'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('11'), side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ map: createTextTexture('12'), side: THREE.DoubleSide }),
    ];

    // Geometría del dodecaedro
    const geometry = new THREE.DodecahedronGeometry(5); // Dodecaedro con radio de 1
    const dodecahedron = new THREE.Mesh(geometry, materials);
    scene.add(dodecahedron);

    // Animación
    const animate = () => {
      requestAnimationFrame(animate);
      dodecahedron.rotation.x += 0.01;
      dodecahedron.rotation.y += 0.01;
      renderer.render(scene, camera);
    };

    animate();

    // Limpiar el efecto
    return () => {
      mount.removeChild(renderer.domElement);
    };
  }, []);

  return <div ref={mountRef} style={{ width, height }} />;
};

export default Dode2;
