import './portada.css';
import { Resultado } from './resultado';


function Portada() {
  return (
    <div className='portada'>
      <h1>DADO INFINITO</h1>
      <Resultado />
    </div>

  )
}

export default Portada
