import React, { useRef, useEffect } from 'react';
import * as THREE from 'three';

const Dodecaedro = ({ width = '500px', height = '500px' }) => {
  const mountRef = useRef(null);

  useEffect(() => {
    const mount = mountRef.current;

    // Escena
    const scene = new THREE.Scene();

    // Cámara
    const camera = new THREE.PerspectiveCamera(75, mount.clientWidth / mount.clientHeight, 0.1, 1000);
    camera.position.z = 3;

    // Renderizador
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(mount.clientWidth, mount.clientHeight);
    renderer.setClearColor(0x00000f, 0.1); // Color de fondo transparente
    mount.appendChild(renderer.domElement);

    // Función para crear un canvas con texto
    const createTextTexture = (text) => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      canvas.width = 256;
      canvas.height = 256;
      context.fillStyle = 'red';
      context.fillRect(0, 0, canvas.width, canvas.height);
      context.fillStyle = 'white';
      context.font = 'bold 150px Arial';
      context.textAlign = 'center';
      context.textBaseline = 'middle';
      context.fillText(text, canvas.width / 2, canvas.height / 2);
      return new THREE.CanvasTexture(canvas);
    };

    // Crear materiales con números
    const loader = new THREE.TextureLoader();
    const materials = [];
    
    for (let i = 1; i <= 12; i++) {
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        canvas.width = 256;
        canvas.height = 256;
    
        context.fillStyle = 'white';
        context.fillRect(0, 0, canvas.width, canvas.height);
    
        context.fillStyle = 'black';
        context.font = '200px Arial';
        context.textAlign = 'center';
        context.textBaseline = 'middle';
        context.fillText(i, canvas.width / 2, canvas.height / 2);
    
        const texture = new THREE.CanvasTexture(canvas);
        materials.push(new THREE.MeshBasicMaterial({ map: texture }));
    }
    
   

    // Crear la geometría del dodecaedro
    const geometry = new THREE.DodecahedronGeometry();
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });

    // Crear la malla del dodecaedro
    const dodecahedron = new THREE.Mesh(geometry, material);
    scene.add(dodecahedron);

    // Función de animación
    function animate() {
      requestAnimationFrame(animate);
      dodecahedron.rotation.x += 0.01;
      dodecahedron.rotation.y += 0.01;
      renderer.render(scene, camera);
    }
    animate();

    // Limpiar el efecto
    return () => {
      mount.removeChild(renderer.domElement);
    };
  }, []);

  return <div ref={mountRef} style={{ width, height }} />;
};

export default Dodecaedro;
