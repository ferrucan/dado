import React, { useState } from 'react'
import './portada.css'
import Sorteo from './sorteo'
import SpinningDice from './dadotresd'



export function Resultado() {

    const [count, setCount] = useState()
    const [result, setResult] = useState(0)



    const handlePlus = () => {

        const resultado = Sorteo(count)
        if (result !== resultado) setResult(resultado)
        else handlePlus()
    }

    return (
        <div className='portada'>
            <form>
                <label>
                    <p>introduce tu numero de posibilidades y pulsa el dado</p>

                    <input
                        className="custom-input"
                        placeholder="Escribe aqui tu numero"
                        value={count}
                        onChange={e => setCount(e.target.value)}
                        type="number"
                    />
                </label>
            </form>
            <div>
                <button onClick={handlePlus}><SpinningDice /></button>
                <p>este es su resultado</p>
                <p>{result}</p>
            </div>

        </div>
    )
}







