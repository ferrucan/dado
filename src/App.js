
import './App.css';
import TableroBingo from './juegos/bingo';

import Cuerpo from './portada/cuerpo';


import { Pie } from './portada/pie';
import Portada from './portada/portada';
import ThreeDTitle3 from'./portada/titulo3'


function App() {
  return (
    <div >
     
  
  <ThreeDTitle3/> 
  <Portada/>
  <TableroBingo/>
  <Cuerpo/>
  <Pie/>
  
    </div>
  );
}

export default App;
