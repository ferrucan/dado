import React, { useState } from "react";

const TableroBingo = () => {
  // Generar los números del 1 al 90
  const numeros = Array.from({ length: 90 }, (_, index) => index + 1);

  // Organizar los números en 9 columnas
  const columnas = Array.from({ length: 9 }, (_, colIndex) =>
    numeros.slice(colIndex * 10, colIndex * 10 + 10)
  );

  // Estados
  const [numerosSeleccionados, setNumerosSeleccionados] = useState([]);
  const [ultimoNumero, setUltimoNumero] = useState(null); // Estado para el último número seleccionado

  // Función para seleccionar un número al azar
  const seleccionarNumero = () => {
    const numerosDisponibles = numeros.filter(
      (num) => !numerosSeleccionados.includes(num)
    );
    if (numerosDisponibles.length === 0) {
      alert("¡Todos los números ya han sido seleccionados!");
      return;
    }
    const numeroAleatorio =
      numerosDisponibles[Math.floor(Math.random() * numerosDisponibles.length)];
    setNumerosSeleccionados([...numerosSeleccionados, numeroAleatorio]);
    setUltimoNumero(numeroAleatorio); // Actualizar el último número seleccionado
  };

  // Función para reiniciar el tablero
  const reiniciarTablero = () => {
    setNumerosSeleccionados([]);
    setUltimoNumero(null); // Restablecer el último número
  };

  return (
    <div style={{ textAlign: "center", fontFamily: "Arial, sans-serif" }}>
      <h1>Tablero de BINGO </h1>
      <div style={{ marginBottom: "20px" }}>
        <button
          onClick={seleccionarNumero}
          style={{
            margin: "10px",
            padding: "10px 20px",
            fontSize: "16px",
            cursor: "pointer",
            borderRadius: "4px",
            backgroundColor: "#4CAF50",
            color: "white",
            border: "none",
          }}
        >
          Seleccionar número
        </button>
        <span
          style={{
            display: "inline-block",
            margin: "0 20px",
            padding: "10px 20px",
            fontSize: "20px",
            fontWeight: "bold",
            color: "#333",
            backgroundColor: "#f0f0f0",
            borderRadius: "8px",
          }}
        >
          {ultimoNumero !== null ? `Último número: ${ultimoNumero}` : "Aún no hay número"}
        </span>
        <button
          onClick={reiniciarTablero}
          style={{
            margin: "10px",
            padding: "10px 20px",
            fontSize: "16px",
            cursor: "pointer",
            borderRadius: "4px",
            backgroundColor: "#FF6347",
            color: "white",
            border: "none",
          }}
        >
          Reiniciar
        </button>
      </div>
      <div style={{ display: "grid", gridTemplateColumns: "repeat(9, auto)", gap: "5px" }}>
        {Array.from({ length: 10 }, (_, rowIndex) =>
          columnas.map((columna, colIndex) => (
            <div
              key={`${colIndex}-${rowIndex}`}
              style={{
                border: "1px solid #ddd",
                padding: "10px",
                backgroundColor: numerosSeleccionados.includes(columna[rowIndex])
                  ? "#FFD700" // Color para números seleccionados
                  : "#f9f9f9",
                textAlign: "center",
                borderRadius: "4px",
                fontWeight: "bold",
              }}
            >
              {columna[rowIndex]}
            </div>
          ))
        )}
      </div>
    </div>
  );
};

export default TableroBingo;
