import React, { useRef, useEffect } from 'react';
import * as THREE from 'three';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader.js';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

const ThreeDTitle3 = () => {
  const mountRef = useRef(null);

  useEffect(() => {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    mountRef.current.appendChild(renderer.domElement);

    // Responsive design
    window.addEventListener('resize', () => {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(window.innerWidth, window.innerHeight);
    });

    // Lighting
    const ambientLight = new THREE.AmbientLight(0x0000ff, 0.5);
    scene.add(ambientLight);
    const pointLight = new THREE.PointLight(0xffff00, 1);
    pointLight.position.set(500, 500, 500);
    scene.add(pointLight);

    // Load font and create 3D text
    const loader = new FontLoader();
    loader.load('https://threejs.org/examples/fonts/helvetiker_regular.typeface.json', (font) => {
      const textGeometry = new TextGeometry('meu Titulo', {
        font: font,
        size: 120,
        height: 50,
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 10,
        bevelSize: 8,
        bevelOffset: 0,
        bevelSegments: 5
      });

      const textMaterial = new THREE.MeshStandardMaterial({ color: 0xffffff });
      const textMesh = new THREE.Mesh(textGeometry, textMaterial);

      // Create a parent object
      const parent = new THREE.Object3D();
      parent.add(textMesh);
      scene.add(parent);

      // Center the geometry
      textGeometry.center();

      // Adjust the position of the text within the parent
      textMesh.position.set(0, 0, 0); // Adjust this to change the rotation point
    

      // Crear la geometría del plano
      const geometryplane = new THREE.PlaneGeometry(800, 300);
      const materialplane = new THREE.MeshBasicMaterial({ color: 0x0f00ff, side: THREE.DoubleSide });
      const plane = new THREE.Mesh(geometryplane, materialplane);
      plane.position.set(0, 0, 0);
      scene.add(plane);

      // Crear la geometría del plano2
      const geometryplane2 = new THREE.PlaneGeometry(300, 300);
      const materialplane2 = new THREE.MeshBasicMaterial({ color: 0xff0000, side: THREE.DoubleSide });
      const plane2 = new THREE.Mesh(geometryplane2, materialplane2);
      plane2.position.set(550, 0, 0);
      scene.add(plane2);

      // Animate text
      const animate = () => {
        requestAnimationFrame(animate);
        parent.rotation.x += 0.01;
        parent.rotation.y += 0.01;
        renderer.render(scene, camera);
      };
      animate();
    });

    // Orbit controls
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;
    camera.position.x = 10;
    camera.position.y = 500;
    camera.position.z = 500;

    return () => {
      mountRef.current.removeChild(renderer.domElement);
    };
  }, []);

  return <div ref={mountRef} />;
};

export default ThreeDTitle3;
